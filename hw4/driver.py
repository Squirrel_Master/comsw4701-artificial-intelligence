# Dallas Jones drj2115
# Columbia University
# COMSW4701 Artificial Intelligence
# Professor Ansaf Salleb-Aouissi
# Homework 4: CSP
# 26 June 2017

import resource
import sys
import time
import re
import copy

# Queue:
# Queue is a basic FIFO queue data structure implementation.
class Queue:
	def __init__(self):
		self.items = []

	def isEmpty(self):
		return self.items == []

	def enqueue(self, item):
		if item != None:
			self.items.insert(0, item)

	def dequeue(self):
		if not self.isEmpty():
			return self.items.pop()
		return None

	def size(self):
		return len(self.items)

	def qprint(self):
		for q in self.items:
			print(q)

	def __contains__(self, item):
		return item in self.items

def lt(var1, var2):
	return var1 < var2
def gt(var1, var2):
	return var1 > var2
def eq(var1, var2):
	return var1 == var2
def neq(var1, var2):
	return var1 != var2

class CSP:
	def __init__(self):
		self.vars = dict()	  # vars['A1'] = True, False for assignment
		self.domains = dict()     # domain['A1'] = (1, 3, 5, 9)
		self.constraints = dict() # constraints['A1'] = {'A2' : cmp_func}
		self.vars_assigned = 0
		self.num_vars = 0

	# insert_variable:
	# Adds the variable label to the CSP's set vars and adds the set of
	# domain values to the CSP's dictionary domain.
	#
	# NOTE: This function assumes all unary constraints have already
	# been determined in the domain set (d_set).  See the notes
	# accompanying insert_constraint (below) for further details.
	def insert_variable(self, v_label, d_set):
		self.domains[v_label] = d_set
		if len(d_set) == 1:
			self.vars_assigned += 1
			self.vars[v_label] = True
		else:
			self.vars[v_label] = False
		self.num_vars += 1
		self.constraints[v_label] = dict()

	# insert_binary_constraint:
	# Appends to the constraints list a tuple consisting of an arc tuple
	# and the said tuple's comparison function.
	#
	# NOTE: This function assumes all unary constraints have already
	# been determined in the domain set.  If this is not the case,
	# then variables' neighbors lists will contain domain values,
	# which is obviously not desired.
	#	Example: Do not add arc_tuple (('A', 3), lt) when you want
	#		 to enforce A < 3.  Calculate this prior and
	#		 update A's domain set appropriately, and THEN
	#		 call insert_variable with 'A' and a domain set
	#		 consisting of values less than 3.
	def insert_binary_constraint(self, v1, v2, cmp_func):
		if cmp_func == None:
			print("Constraints require a comparison function.")
			return

		self.constraints[v1][v2] = cmp_func

	def delete_from_domain(self, v_label, d_val):
		self.domains[v_label].discard(d_val)
		return len(self.domains[v_label])

	def ac3(self):
		q = Queue()
		for v in self.constraints:
			for x in self.constraints[v]:
				q.enqueue((v, x, self.constraints[v][x]))
		while not q.isEmpty():
			c = q.dequeue()
			Xi = c[0]
			Xj = c[1]
			if self.revise(c):
				if len(self.domains[Xi]) < 1:
					return False
				for Xk in self.constraints[Xi]:
					q.enqueue((Xk, Xi, self.constraints[Xk][Xi]))
		return True

	def revise(self, constraint):
		revised = False
		Xi = constraint[0]
		Xj = constraint[1]
		cmp_func = constraint[2]
		del_list = []
		for x in self.domains[Xi]:
			satisfied = False
			for y in self.domains[Xj]:
				if cmp_func(x, y):
					satisfied = True
					break
			if not satisfied:
				revised = True
				del_list.append(x)

		for x in del_list:
			self.delete_from_domain(Xi, x)
		return revised

	# Rather than checking merely by modifying domains, values are filled in self.vars
	def backtrack(self):
		# If assignment is complete, we're done
		if self.vars_assigned == self.num_vars:
			return True
		var = self.mrv()
		prev_domains = copy.deepcopy(self.domains)
		prev_vars = copy.deepcopy(self.vars)
		prev_assigned = copy.deepcopy(self.vars_assigned)

		cpy_domain = copy.deepcopy(self.domains[var])

		# For each value in the domain of var
		for v in cpy_domain:
			# Check if the value is consistent
			if self.check_consistent(var, v) == True:
				# Assign the current variable
				self.vars[var] = v
				self.vars_assigned += 1
				self.domains[var].clear()
				self.domains[var].add(v)
				# Perform forward checking -> get inferences
				if self.fc(var, v) == True:
					if self.backtrack() == True:
						return True

				# Undo assignment and inferences
				self.vars = copy.deepcopy(prev_vars)
				self.domains = copy.deepcopy(prev_domains)
				self.vars_assigned = copy.deepcopy(prev_assigned)
		return False

	def check_consistent(self, var, val):
		# For each neighbor
		for Xi in self.constraints[var]:
			satisfied = False
			cmp_func = self.constraints[var][Xi]
			for y in self.domains[Xi]:
				if cmp_func(val, y) == True:
					satisfied = True
					break
			if satisfied == False:
				return False
		return True

	def fc(self, var, val):
		for Xi in self.constraints[var]:
			cmp_func = self.constraints[Xi][var]
			d_set = set()
			for y in self.domains[Xi]:
				if cmp_func(y, val) == False:
					d_set.add(y)
			for d in d_set:
				if self.delete_from_domain(Xi, d) == 0:
					return False
		return True

	def mrv(self):
		min = sys.maxint
		select_var = None
		for v in self.vars:
			if self.vars[v]: # If already assigned, continue
				continue
			d_len = len(self.domains[v])
			if d_len < min:
				select_var = v
				min = d_len
		return select_var

def main():
	if len(sys.argv) < 2:
		print("Usage: %s <input_string>" % sys.argv[0])
		sys.exit(-1)

	chars = list(sys.argv[1])
	if len(chars) != 81:
		print("%s len: %d" % (sys.argv[1], len(chars)))
		print("Invalid input string!")
		sys.exit(-1)

	csp = CSP()

	# Add variables and their domains to the CSP
	for row in range(0, 9):
		row_v = chr(ord('A') + row)
		for col in range(0, 9):
			v = "%s%d" % (row_v, col)
			d = ord(chars[row * 9 + col]) - ord('0')

			set_d = set()
			# Set the variable's domain to full set 1-9
			if d == 0:
				for i in range(1, 10):
					set_d.add(i)

			# Otherwise, handle unary constraints (set domain to
			# the already assigned single value).
			else:
				set_d.add(d)
			csp.insert_variable(v, set_d)

	# Add constraints to the CSP
	for v1 in csp.vars:
		for v2 in csp.vars:
			if v2 == v1:
				continue
			l1 = list(v1)
			l2 = list(v2)

			box_col1 = (ord(l1[1]) - ord('0'))/ 3
			box_row1 = (ord(l1[0]) - ord('A')) / 3

			box_col2 = (ord(l2[1]) - ord('0'))/ 3
			box_row2 = (ord(l2[0]) - ord('A')) / 3

			if l1[0] == l2[0] or l1[1] == l2[1] or \
				(box_col1 == box_col2 and box_row1 == box_row2):
				csp.insert_binary_constraint(v1, v2, neq)

	start_time = time.time()
	csp.ac3()
	csp.backtrack()
	end_time = time.time() - start_time

	solution_str = ""
	solved = True
	for row in range(0, 9):
		row_v = chr(ord('A') + row)
		for col in range(0, 9):
			v = "%s%d" % (row_v, col)
			d_set = csp.domains[v]
			if len(d_set) == 1:
				for x in d_set:
					solution_str += chr(x + ord('0'))
			else:
				solution_str += '0'
				solved = False

	outfile = open("output.txt", 'a')
	if solved:
		print >> outfile, "%s Solved in %fs" % (solution_str, end_time)
	else:
		print >> outfile, "%s Unsolved in %fs" % (solution_str, end_time)

if __name__ == "__main__":
	main()
