# Dallas Jones drj2115
# Columbia University
# COMSW4701 Artificial Intelligence
# Professor Ansaf Salleb-Aouissi
# Homework 1: Search
# 4 June 2017

import resource
import sys
import time

goal_test = (0, 1, 2, 3, 4, 5, 6, 7, 8)
cost_of_path = 0
nodes_expanded = 0
search_depth = 0
max_search_depth = 0

# Queue:
# Queue is a basic FIFO queue data structure implementation.
class Queue:
	def __init__(self):
		self.items = []

	def isEmpty(self):
		return self.items == []

	def enqueue(self, item):
		if item != None:
			self.items.insert(0, item)

	def dequeue(self):
		if not self.isEmpty():
			return self.items.pop()
		return None

	def size(self):
		return len(self.items)

	def qprint(self):
		for q in self.items:
			print(q)

	def __contains__(self, item):
		return item in self.items

# Stack:
# Stack is a basic LIFO stack data structure implementation.
class Stack:
	def __init__(self):
		self.items = []

	def isEmpty(self):
		return self.items == []

	def push(self, item):
		if item != None:
			self.items.append(item)

	def pop(self):
		if not self.isEmpty():
			return self.items.pop()
		return None

	def size(self):
		return len(self.items)

	def sprint(self):
		for s in self.items:
			print(s)

	def __contains__(self, item):
		return item in self.items

# PriorityQueue:
# PriorityQueue is a minheap implementation of a classic binary heap.
class PriorityQueue:
	def __init__(self):
		self.items = [0]
		self.size = 0

	def isEmpty(self):
		return self.size == 0

	def min(self, i):
		j = i * 2
		if (j + 1) > self.size:
			return j
		if self.items[j] < self.items[j + 1]:
			return j
		return j + 1

	def percolate_up(self, i):
		j = i // 2
		while j > 0:
			if self.items[i] < self.items[j]:
				tmp = self.items[j]
				self.items[j] = self.items[i]
				self.items[i] = tmp
			i = j
			j //= 2

	def percolate_down(self, i):
		while (i * 2) <= self.size:
			j = self.min(i)
			if self.items[i] > self.items[j]:
				tmp = self.items[i]
				self.items[i] = self.items[j]
				self.items[j] = tmp
			i = j

	def delete_min(self):
		if self.size < 1:
			return None
		ret = self.items[1]
		self.items[1] = self.items[self.size]
		self.size -= 1
		self.items.pop()
		self.percolate_down(1)
		return ret

	def insert(self, item):
		self.size += 1
		self.items.append(item)
		self.percolate_up(self.size)

	def __contains__(self, item):
		return item in self.items

# display_board:
# Takes a tuple of length 9+ and prints all 9 values of an 8-puzzle state.
def display_board(state):
	if isinstance(state, tuple)  and len(state) >= 9:
		print("-------\n|%d|%d|%d|\n|%d|%d|%d|\n|%d|%d|%d|\n-------" %
			(state[0], state[1], state[2], state[3], state[4],
			state[5], state[6], state[7], state[8]))

# move_down:
# Swaps the hole with the tile below it (if possible) and returns the
# new state.
def move_down(state):
	if not isinstance(state, tuple) or len(state) < 9:
		return None

	# tuples are immutable, so a conversion to and from list is made.
	new_state = list(state[:])
	index = new_state.index(0)
	if index < 6:
		temp = new_state[index + 3]
		new_state[index + 3] = new_state[index]
		new_state[index] = temp
		return tuple(new_state)

	return None

# move_up:
# Swaps the hole with the tile above it (if possible) and returns the
# new state.
def move_up(state):
	if not isinstance(state, tuple) or len(state) < 9:
		return None

	# tuples are immutable, so a conversion to and from list is made.
	new_state = list(state[:])
	index = new_state.index(0)
	if index > 2:
		temp = new_state[index - 3]
		new_state[index - 3] = new_state[index]
		new_state[index] = temp
		return tuple(new_state)

	return None

# move_right:
# Swaps the hole with the tile to its right (if possible) and returns the
# new state.
def move_right(state):
	if not isinstance(state, tuple) or len(state) < 9:
		return None

	# tuples are immutable, so a conversion to and from list is made.
	new_state = list(state[:])
	index = new_state.index(0)
	if not index in [2, 5, 8]:
		temp = new_state[index + 1]
		new_state[index + 1] = new_state[index]
		new_state[index] = temp
		return tuple(new_state)

	return None

# move_left:
# Swaps the hole with the tile to its left (if possible) and returns the
# new state.
def move_left(state):
	if not isinstance(state, tuple) or len(state) < 9:
		return None

	# tuples are immutable, so a conversion to and from list is made.
	new_state = list(state[:])
	index = new_state.index(0)
	if index % 3 > 0:
		temp = new_state[index - 1]
		new_state[index - 1] = new_state[index]
		new_state[index] = temp
		return tuple(new_state)

	return None

# Node:
# Node is part of a larger graph/tree and contains useful information for
# quick calculation of pathing and so forth.
class Node:
	def __init__(self, state, parent, operator, depth, cost):
		self.state = state
		self.parent = parent
		self.operator = operator
		self.depth = depth
		self.cost = cost

	# expand:
	# Returns a list of neighbor states by trying each available move
	# (except, of course, a move that would return to the parent's state).
	def expand(self):
		expanded_nodes = []

		if self.operator != 'D':
			state = move_up(self.state)
			if state != None:
				expanded_nodes.append(Node(state, self, 'U',
							self.depth + 1, 0))

		if self.operator != 'U':
			state = move_down(self.state)
			if state != None:
				expanded_nodes.append(Node(state, self, 'D',
							self.depth + 1, 0))

		if self.operator != 'R':
			state = move_left(self.state)
			if state != None:
				expanded_nodes.append(Node(state, self, 'L',
							self.depth + 1, 0))

		if self.operator != 'L':
			state = move_right(self.state)
			if state != None:
				expanded_nodes.append(Node(state, self, 'R',
							self.depth + 1, 0))

		return expanded_nodes

	# Comparison operators:
	# < and > compare by node costs. == and != compare by node states.
	# <= and >= combine the above comparisons.
	def __eq__(self, other):
		if isinstance(other, Node):
			return self.state == other.state
		return False

	def __lt__(self, other):
		if isinstance(other, Node):
			return self.cost < other.cost
		return False

	def __le__(self, other):
		if not isinstance(other, Node):
			return False
		if self.cost < other.cost:
			return True
		return self.state == other.state

	def __ne__(self, other):
		return not (self == other)

	def __gt__(self, other):
		if isinstance(other, Node):
			return self.cost > other.cost
		return False

	def __ge__(self, other):
		if not isinstance(other, Node):
			return False
		if self.cost > other.cost:
			return True
		return self.state == other.state

	def __hash__(self):
		return hash(self.state)

# bfs:
# Performs a breadth-first search from a given starting state to a desired
# goal state.  When complete, the final node in the path is returned and
# must be iterated backwards via its parent to discern the path.
# A Queue is utilized for the frontier and stores nodes, but for faster
# comparisons at the cost of space, a copy of its state is stored in
# a set f_map.  In the future, such redundancy shall be rectified.
def bfs(start_state, goal):
	global nodes_expanded
	global search_depth
	global max_search_depth

	nodes_expanded = 0
	search_depth = 0
	max_search_depth = 0

	frontier = Queue()
	frontier.enqueue(Node(start_state, None, None, 0, 0))

	# Takes up more memory, but hashing states is far faster than
	# linear lookup in a list
	f_map = {start_state}
	explored = set()

	while not frontier.isEmpty():
		node = frontier.dequeue()
		f_map.discard(node.state)
		search_depth = node.depth

		if goal == node.state:
			return node

		explored.add(node.state)
		nodes_expanded += 1

		if search_depth + 1 > max_search_depth:
			max_search_depth = search_depth + 1

		for e in node.expand():
			found = 0
			if not (e.state in f_map or e.state in explored):
				# optimization:
				# if e.state == goal:
				#	return e
				f_map.add(e.state)
				frontier.enqueue(e)
	return None

# dfs:
# Performs a depth-first search on a given starting state to a desired goal
# state.  As with bfs, a copy of each frontier node's state is stored in a
# set f_map for faster lookups.
def dfs(start_state, goal):
	global nodes_expanded
	global search_depth
	global max_search_depth

	nodes_expanded = 0
	search_depth = 0
	max_search_depth = 0

	frontier = Stack()
	frontier.push(Node(start_state, None, None, 0, 0))

	f_map = {start_state}
	explored = set()

	while not frontier.isEmpty():
		node = frontier.pop()
		f_map.discard(node.state)
		search_depth = node.depth

		if goal == node.state:
			return node

		explored.add(node.state)
		nodes_expanded += 1
		if search_depth + 1 > max_search_depth:
			max_search_depth = search_depth + 1

		# Note: to comply with the homework rules, the list of expanded
		# nodes has been reversed to ensure UDLR movement order.
		for e in reversed(node.expand()):
			found = 0
			if e.state in f_map:
				continue

			if e.state not in explored:
				# optimization:
				# if e.state == goal:
				#	return e
				f_map.add(e.state)
				frontier.push(e)
	return None

# m_dist:
# Computes the Manhattan distance between two states' values. This entails
# calculating how far EACH tile in the state is from its corresponding
# position in the goal state.
def m_dist(state, goal):
	dist = 0
	sz = len(state)
	if sz != 9 or sz != len(goal):
		return None

	for i in range(0, sz):
		if state[i] == goal[i]:
			continue
		r1 = i // 3
		c1 = i % 3
		r2 = 0
		c2 = 0
		for j in range(0, sz):
			if goal[j] == state[i]:
				r2 = j // 3
				c2 = j % 3
				break
		if r1 > r2:
			r1 -= r2
		else:
			r1 = r2 - r1

		if c1 > c2:
			c1 -= c2
		else:
			c1 = c2 - c1

		dist += r1 + c1

	return dist

# a_star:
# Performs an A* search on a given starting state to a desired goal state.
# The heuristic used is a Manhattan distance (more specifically, each
# state's Manhattan distance is the sum of each tile's Manhattan distance
# from its corresponding position in the goal state).
def a_star(start_state, goal):
	global nodes_expanded
	global search_depth
	global max_search_depth

	nodes_expanded = 0
	search_depth = 0
	max_search_depth = 0

	frontier = PriorityQueue()
	frontier.insert(Node(start_state, None, None, 0, 0))
	explored = set()
	g_score = {start_state : 0}
	f_score = {start_state : 0}

	while not frontier.isEmpty():
		node = frontier.delete_min()
		search_depth = node.depth
		if node.state == goal:
			return node
		explored.add(node.state)

		if search_depth + 1 > max_search_depth:
			max_search_depth = search_depth + 1

		nodes_expanded += 1
		for e in node.expand():
			if e.state in explored:
				continue
			tmp_g_score = g_score[node.state] + 1
			e.cost = tmp_g_score + m_dist(e.state, goal)
			if not e in frontier:
				frontier.insert(e)
			elif tmp_g_score >= g_score[e.state]:
				continue
			g_score[e.state] = tmp_g_score
			f_score[e.state] = e.cost
	return None

# main:
# Read the desired algorithm and starting state from the command line,
# compute the path, and output the results to output.txt.
# Example usage:
#	python driver.py bst 8,6,3,4,1,0,5,7,2
def main():
	if len(sys.argv) < 3:
		print("Usage: %s <ast/bfs/dfs> <state>" % sys.argv[0])
		sys.exit(-1)

	# Convert the input state, a string, to a list of values.
	zero = ord('0')
	start_state_list = sys.argv[2].split(",")
	for i in range(0, len(start_state_list)):
		start_state_list[i] = ord(start_state_list[i]) - zero

	# Convert the list to a tuple.
	starting_state = tuple(start_state_list)

	# Check if the puzzle is solvable (number of inversions must be even).
	inv_count = 0
	for i in range(0, 8):
		for j in range(i + 1, 9):
			if ((starting_state[j] != 0) and
				(starting_state[i] > starting_state[j])):
				inv_count += 1

	if inv_count % 2 != 0:
		print("The puzzle is unsolvable.")
		sys.exit(-1)

	# Start timing.
	duration = time.time()

	if sys.argv[1] == "ast":
		node = a_star(starting_state, goal_test)
	elif sys.argv[1] == "bfs":
		node = bfs(starting_state, goal_test)
	elif sys.argv[1] == "dfs":
		node = dfs(starting_state, goal_test)
	else:
		print("Invalid algorithm '%s'. Please use 'ast', 'bfs', or " \
			"'dfs'." % sys.argv[1])
		sys.exit(-1)

	# Stop timing.
	duration = time.time() - duration

	# Compute the path by iterating from the node up the tree to the
	# starting node.
	path_to_goal = []
	while node != None and node.operator != None:
		if node.operator == 'U':
			path_to_goal.append('Up')
		elif node.operator == 'D':
			path_to_goal.append('Down')
		elif node.operator == 'L':
			path_to_goal.append('Left')
		else:
			path_to_goal.append('Right')
		node = node.parent

	path_to_goal.reverse()

	# Append results to the output file.
	file = open('./output.txt', 'a')
	print >> file, "%s %s:" % (sys.argv[1], sys.argv[2])
	print >> file, (path_to_goal)
	print >> file, "cost_of_path: %d" % len(path_to_goal)
	print >> file, "nodes_expanded: %d" % nodes_expanded
	print >> file, "search_depth: %d" % search_depth
	print >> file, "max_search_depth: %d" % max_search_depth
	print >> file, "running_time: %f" % duration
	print >> file, "max_ram_usage: %f\n" % \
		(resource.getrusage(resource.RUSAGE_SELF).ru_maxrss / 1024.0)
	file.close()

if __name__ == "__main__":
	main()
