import sys
import time
from math import log
from math import pow
from random import randint
from BaseAI import BaseAI

max_int = sys.maxint
min_int = -max_int

class PlayerAI(BaseAI):
	global max_int
	global min_int

	def __init__(self):
		self.dir = 0
		self.start_time = 0
		self.decrease_max_depth = False
		self.max_depth = 16
		self.wt_pow = [[0 for x in range(4)] for y in range(4)]
		self.wt_base = [[0 for x in range(4)] for y in range(4)]

		self.wt_pow[0][0] = 16
		self.wt_pow[0][1] = 15
		self.wt_pow[0][2] = 14
		self.wt_pow[0][3] = 13
		self.wt_pow[1][3] = 12
		self.wt_pow[1][2] = 11
		self.wt_pow[1][1] = 10
		self.wt_pow[1][0] = 9
		self.wt_pow[2][0] = 8
		self.wt_pow[2][1] = 7
		self.wt_pow[2][2] = 6
		self.wt_pow[2][3] = 5
		self.wt_pow[3][3] = 4
		self.wt_pow[3][2] = 3
		self.wt_pow[3][1] = 2
		self.wt_pow[3][0] = 1

		self.wt_base[0][0] = 1.0
		self.wt_base[1][0] = 6.0/7.0 * .75
		self.wt_base[0][1] = 6.0/7.0
		self.wt_base[2][0] = 5.0/7.0 * .5
		self.wt_base[1][1] = 5.0/7.0 * .75
		self.wt_base[0][2] = 5.0/7.0
		self.wt_base[3][0] = 4.0/7.0 * .25
		self.wt_base[2][1] = 4.0/7.0 * .5
		self.wt_base[1][2] = 4.0/7.0 * .75
		self.wt_base[0][3] = 4.0/7.0
		self.wt_base[3][1] = 3.0/7.0 * .25
		self.wt_base[2][2] = 3.0/7.0 * .5
		self.wt_base[1][3] = 3.0/7.0 * .75
		self.wt_base[3][2] = 2.0/7.0 * .25
		self.wt_base[2][3] = 2.0/7.0 * .5
		self.wt_base[3][3] = 1.0/7.0 * .25

	# eval:
	# Basic heuristic function that assigns a value to a grid based on
	# the diagonal distance from the top-left corner and vertical
	# distance from the top row. Originally, only the former was used,
	# but the need to weight in terms of a particular row is necessary
	# because diagonal moves are not allowed.
	#
	# For a cell[x, y] d diagonals and v verticals away from the top-left
	# and top row, respectively:
	#	weight[x,y] = value(cell[x,y]) * (d / 7) * ((4 - v) / 4)
	def eval(self, grid):
		ret = 0.0
		for i in range(0, 3):
			for j in range(0, 3):
				if grid.map[i][j] > 0:
					ret += pow(log(grid.map[i][j], 2) * self.wt_base[i][j], self.wt_pow[i][j])
		return ret

	def get_move(self, grid, alpha, beta, depth):
		curr_time = time.clock() - self.start_time
		if curr_time > .2:
			self.decrease_max_depth = True
			return [self.dir, self.eval(grid)]

		if depth == self.max_depth:
			return [self.dir, self.eval(grid)]

		# maximizing:
		if depth % 2 == 0:
			max_util = min_int
			moves = grid.getAvailableMoves()
			if moves == []:
				return [self.dir, alpha]
			max_child = moves[0]
			for mv in moves:
				g = grid.clone()
				g.move(mv)
				r = self.get_move(g, alpha, beta, depth + 1)
				if r[1] > max_util:
					max_util = r[1]
					max_child = mv

				if self.decrease_max_depth:
					break

				if max_util >= beta:
					break

				if max_util > alpha:
					alpha = max_util

			self.dir = max_child
			return [max_child, max_util]

		# minimizing:
		# In this case, we compare against grids given a possible
		# computer tile placement and minimize accordingly.
		else:
			min_util = max_int
			min_child = self.dir
			cells = grid.getAvailableCells()
			if cells == []:
				return [self.dir, beta]
			g = grid.clone()
			if randint(0, 10):
				next_val = 2
			else:
				next_val = 4

			for c in cells:
				g.setCellValue(c, next_val)
				r = self.get_move(g, alpha, beta, depth + 1)
				if r[1] < min_util:
					min_util = r[1]
					min_child = r[0]

				if min_util <= alpha:
					break

				if min_util < beta:
					beta = min_util
				g.setCellValue(c, 0)

				if self.decrease_max_depth:
					break

			self.dir = min_child
			return [min_child, min_util]

	def getMove(self, grid):
		self.start_time = time.clock()
		ret = self.get_move(grid, min_int, max_int, 0)

		if self.decrease_max_depth:
			if self.max_depth > 1:
				self.max_depth -= 1

			self.decrease_max_depth = False

		return ret[0]
