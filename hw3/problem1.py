# Dallas Jones drj2115
# Columbia University
# COMSW4701 Artificial Intelligence
# Professor Ansaf Salleb-Aouissi
# Homework 3: Machine Learning
# 16 June 2017

import sys
import re

# yhat:
# Takes a list row of the form [x1, ..., xd, y] and a list w of the form
# [w1, ..., wd, b] and computes the equation f(x) = w0 + x1 * w1 + ... xd * wd.
# If this sum is greater than or equal to zero, 1.0 is returned, otherwise -1.0
# since the prediction is based on binary classification.
def yhat(row, w):
	d = len(row)
	if d != len(w):
		return None

	y = w[d - 1]
	for i in range(0, d - 1):
		y += w[i] * row[i]

	if y > 0.0:
		return 1.0
	return -1.0

# train:
# Takes in training data (examples) and continually adjusts the weights until
# convergence (as tracked via err_sum).  The assumption is that the training
# data is linearly separable and each row is of the form [x1, ..., xd, y].
# Weights are calculated and stored in a list of the form [w1, ..., wd, b]
# where b is the bias/offset/intercept, also commonly denoted by w0. As
# per the assignment instructions, each calculation of the weights is printed
# to an output file.
def train(training_data, rate, outfile):
	d = len(training_data[0])
	w = [0.0 for i in range(d)]

	err_sum = 1
	while err_sum != 0:
		err_sum = 0
		for row in training_data:
			err = row[d - 1] - yhat(row, w)
			if err != 0:
				err_sum += 1
			w[d - 1] += rate * err
			for i in range(d - 1):
				w[i] += rate * err * row[i]
		
		print >> outfile, ",".join([str(a) for a in w])
	return w

def is_float(value):
	try:
		float(value)
		return True
	except:
		return False

def main():
	if len(sys.argv) < 2:
		print("Usage: python %s <input csv> <output csv>" % sys.argv[0])
		sys.exit(-1)

	infile = open(sys.argv[1], 'r')

	# Read in data line by line and convert all values to floats.
	data = []

	lines = re.split('[\r\n]', infile.read())
	infile.close()

	for line in lines:
		not_float = False
		l = line.split(",")
		for i in range(0, len(l)):
			if is_float(l[i]) == False:
				not_float = True
				break
			l[i] = float(l[i])
		if not_float:
			continue
		data.append(l)

	outfile = None
	if len(sys.argv) < 3:
		outfile = open('./output1.csv', 'w')
	else:
		outfile = open(sys.argv[2], 'w')

	# Calculate the weights by training them on the example data.
	w = train(data, .2, outfile)

	outfile.close()

if __name__ == "__main__":
	main()
