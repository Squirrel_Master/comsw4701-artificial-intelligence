# Dallas Jones drj2115
# Columbia University
# COMSW4701 Artificial Intelligence
# Professor Ansaf Salleb-Aouissi
# Homework 3: Machine Learning
# 16 June 2017

import sys
import re

def is_float(value):
	try:
		float(value)
		return True
	except:
		return False

# yhat:
def yhat(row, w):
	# last value in row is the actual y
	d = len(row)
	if d != len(w):
		return None
	y = w[d - 1]
	for i in range(0, d - 1):
		y += w[i] * row[i]
	return y

# train:
def train(X, rate, num_iter, outfile):
	d = len(X[0])
	w = [0.0 for i in range(d)]

	R_prev = float("inf")
	R = -float("inf")
	for iterations in range(num_iter):
		errs = []
		betas = []
		b = 0.0
		for row in X:
			err = yhat(row, w) - row[d - 1]
			errs.append(err)
			b += err

		# bias
		w[d - 1] -= rate * b / float(len(X))

		for e in errs:
			R += e * e
		R /= 2.0 * float(len(X))

		for i in range(d - 1):
			b_sum = 0.0
			for j in range(len(X)):
				b_sum += errs[j] * X[j][i]
			w[i] -= rate * b_sum / float(len(X))

		if R_prev - R == 0.0:
			break

		R_prev = R
		R = 0.0


	print >> outfile, "%.3f, %d, %s" % (rate, iterations + 1, \
					", ".join([str(a) for a in w]))

	return w

def mean(training_data, col_num):
	if (len(training_data[0]) - 1) < col_num:
		return None

	mean = 0.0
	for row in training_data:
		mean += row[col_num]
	return mean / float(len(training_data))

def sdev(training_data, col_num, avg):
	if (len(training_data[0]) - 1) < col_num:
		return None

	# Compute variance
	variance = 0.0
	for row in training_data:
		variance += (row[col_num] - avg) ** 2
	variance /= float(len(training_data))

	return variance ** .5

def scale(training_data, col_num):
	avg = mean(training_data, col_num)
	sd = sdev(training_data, col_num, avg)
	for row in training_data:
		row[col_num] -= avg
		row[col_num] /= sd

	return training_data

def main():
	if len(sys.argv) < 2:
		print("Usage: python %s <input csv> <output csv>" % sys.argv[0])
		sys.exit(-1)

	infile = open(sys.argv[1], 'r')

	# Read in data line by line and convert all values to floats.
	data = []

	lines = re.split('[\r\n]', infile.read())
	infile.close()

	for line in lines:
		not_float = False
		l = line.split(",")
		for i in range(0, len(l)):
			if is_float(l[i]) == False:
				not_float = True
				break
			l[i] = float(l[i])

		if not_float:
			continue
		data.append(l)

	data = scale(data, 0)
	data = scale(data, 1)

	outfile = None
	if len(sys.argv) < 3:
		outfile = open('./output2.csv', 'w')
	else:
		outfile = open(sys.argv[2], 'w')

	# Calculate the weights by training them on the example data. Try
	# with different rates to compare the rates of convergence.
	rates = [0.001, 0.005, 0.01, 0.05, 0.1, 0.5, 1.0, 5.0, 10.0, 0.993]
	for alpha in rates:
		w = train(data, alpha, 100, outfile)

	outfile.close()

if __name__ == "__main__":
	main()
