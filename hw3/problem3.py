# Dallas Jones drj2115
# Columbia University
# COMSW4701 Artificial Intelligence
# Professor Ansaf Salleb-Aouissi
# Homework 3: Machine Learning
# 16 June 2017

import sys
import numpy
import re
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import classification_report
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.gaussian_process.kernels import RBF
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier

def is_float(value):
	try:
		float(value)
		return True
	except:
		return False

seed = numpy.random.seed(22)
def main():
	if len(sys.argv) < 2:
		print("Usage: python %s <input csv> <output csv>" % sys.argv[0])
		sys.exit(-1)

	infile = open(sys.argv[1], 'r')

	# Read in data line by line and convert all values to floats.
	data_x = []
	data_y = []

	# Split by new lines (\n) and carriage returns (\r)
	lines = re.split('[\r\n]',infile.read())

	# Separate data in each line into data_x and data_y.  Ensure (via
	# is_float()) that only valid floats are inserted (ie, not letters
	# or words).
	for line in lines:
		not_float = False
		l = line.split(",")
		tmp = []
		for i in range(0, len(l) - 1):

			# Ensure we're not reading in nonsense chars.
			if is_float(l[i]) == False:
				not_float = True
				break
			tmp.append(float(l[i]))
		if not_float:
			continue

		if is_float(l[-1]) == False:
			continue
		data_y.append(float(l[-1]))
		data_x.append(tmp)
	infile.close()

	outfile = None
	if len(sys.argv) < 3:
		outfile = open('./output3.csv', 'w')
	else:
		outfile = open(sys.argv[2], 'w')


	x_train, x_test, y_train, y_test = train_test_split(data_x,
							    data_y,
							    train_size = .6,
							    test_size = .4,
							    stratify = data_y)

	# Possible nearest neighbors is the degrees of freedom
	# (len(x_train) - 1) - degrees of freedom of features (1)
	knneighbors = []
	num_neighbors = min(50, len(x_train) - len(x_train[0]) - 2)
	for i in range(1, num_neighbors + 1):
		knneighbors.append(i)

	knnleafs = []
	for i in range(5,65,5):
		knnleafs.append(i)

	maxdepth = []
	minsamplessplit = []
	for i in range(1, 51):
		maxdepth.append(i)
	for i in range(2, 12, 2):
		minsamplessplit.append(i)

	# Set random_state to ensure repeated results for each test
	rstate = 100

	SVC_linear = [{'C'            : [.1, .5, 1, 5, 10, 50, 100],
		       'kernel'       : ['linear'],
		       'random_state' : [rstate]}]
	SVC_polynomial = [{'C'            : [.1, 1, 3],
			   'degree'       : [4, 5, 6],
			   'gamma'        : [.1, .5],
			   'kernel'       : ['poly'],
			   'random_state' : [rstate]}]
	SVC_rbf = [{'C'            : [.1, .5, 1, 5, 10, 50, 100],
		    'gamma'        : [.1,.5],
		    'kernel'       : ['rbf'],
		    'random_state' : [rstate]}]
	SVC_logistic = [{'C'            : [.1, .5, 1, 5, 10, 50, 100],
			 'random_state' : [rstate]}]
	SVC_knn = [{'n_neighbors' : knneighbors,
		    'leaf_size'   : knnleafs}]
	SVC_dtree = [{'max_depth'         : maxdepth,
		      'min_samples_split' : minsamplessplit,
		      'random_state'      : [rstate]}]
	SVC_rforest = [{'max_depth'         : maxdepth,
			'min_samples_split' : minsamplessplit,
			'random_state'      : [rstate]}]

	num_folds = 5
	score_type = ['accuracy'] # should also compare 'precision' and 'recall'

	scores = []
	scores.append(['SVM with Linear Kernel'])
	scores.append(['SVM with Polynomial Kernel'])
	scores.append(['SVM with RBF kernel'])
	scores.append(['Logistic Regression'])
	scores.append(['k-Nearest Neighbors'])
	scores.append(['Decision Trees'])
	scores.append(['Random Forest'])

	# Stemming from the example from <scikit-learn.org/stable/auto_examples
	# /model_selection/grid_search_digits.html>, iterate through the
	# scoring types (in this case, only 'accuracy') to tune the hyper
	# parameters (such as the number of neighbors, leaves, etc.).
	# Then, using the parameters with the best accuracy score, train the
	# training-betas (x_train).  Finally, compute the score utilizing
	# the trained estimator against the testing dataset (x_test, y_test).
	for s in score_type:

		# Tune the hyper-parameters.
		# Note: n_jobs = -1 attempts to split the work amongst the
		# cpus.
		clf_linear = GridSearchCV(SVC(),
					  SVC_linear,
					  cv = num_folds,
					  scoring = s,
					  n_jobs = -1)
		clf_polynomial = GridSearchCV(SVC(),
					      SVC_polynomial,
					      cv = num_folds,
					      scoring = s,
					      n_jobs = -1)
		clf_rbf = GridSearchCV(SVC(),
				       SVC_rbf,
				       cv = num_folds,
				       scoring = s,
				       n_jobs = -1)
		clf_logistic = GridSearchCV(LogisticRegression(),
					    SVC_logistic,
					    cv = num_folds,
					    scoring = s,
					    n_jobs = -1)
		clf_knn = GridSearchCV(KNeighborsClassifier(),
				       SVC_knn,
				       cv = num_folds,
				       scoring = s,
				       n_jobs = -1)
		clf_dtree = GridSearchCV(DecisionTreeClassifier(),
					 SVC_dtree,
					 cv = num_folds,
					 scoring = s,
					 n_jobs = -1)
		clf_rforest = GridSearchCV(RandomForestClassifier(),
					   SVC_rforest,
					   cv = num_folds,
					   scoring = s,
					   n_jobs = -1)

		# Fit the training data.
		clf_linear.fit(x_train,y_train)
		clf_polynomial.fit(x_train, y_train)
		clf_rbf.fit(x_train,y_train)
		clf_logistic.fit(x_train, y_train)
		clf_knn.fit(x_train, y_train)
		clf_dtree.fit(x_train, y_train)
		clf_rforest.fit(x_train, y_train)

		# Add the best training score and the testing score.
		scores[0].append(clf_linear.best_score_)
		scores[0].append(clf_linear.best_estimator_.score(x_test, y_test))

		scores[1].append(clf_polynomial.best_score_)
		scores[1].append(clf_polynomial.best_estimator_.score(x_test, y_test))

		scores[2].append(clf_rbf.best_score_)
		scores[2].append(clf_rbf.best_estimator_.score(x_test, y_test))

		scores[3].append(clf_logistic.best_score_)
		scores[3].append(clf_logistic.best_estimator_.score(x_test, y_test))

		scores[4].append(clf_knn.best_score_)
		scores[4].append(clf_knn.best_estimator_.score(x_test, y_test))

		scores[5].append(clf_dtree.best_score_)
		scores[5].append(clf_dtree.best_estimator_.score(x_test, y_test))

		scores[6].append(clf_rforest.best_score_)
		scores[6].append(clf_rforest.best_estimator_.score(x_test, y_test))

	# Print the scores to the output file.
	for s in scores:
		print >> outfile, "%s" % ", ".join([str(a) for a in s])

	outfile.close()

if __name__ == "__main__":
	main()

